import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'usertodos',
  templateUrl: './usertodos.component.html',
  styleUrls: ['./usertodos.component.css']
})
export class UsertodosComponent implements OnInit {
  name;
  show1=false;
  todos = [{"text":"study json", "id":1}
          ,{"text":"make project", "id":2}
          ,{"text":"buy milk", "id":3}] 

  constructor() { }

  ngOnInit() {
    console.log(this.show1)

  }
  showname(){
    this.show1=true;
    console.log(this.show1)
  }

}
