import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  @Input() data:any;
  @Output() waystod= new EventEmitter<any>();
  name;
  id;
  constructor() { }

  ngOnInit() {
    this.name=this.data.text;
    this.id=this.data.id;
  }
  delete(){
   // console.log(this.name);
    this.waystod.emit(this.name);
  }
}
