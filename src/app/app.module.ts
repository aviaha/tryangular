import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { NavComponent } from './nav/nav.component';
import { TodosComponent } from './todos/todos.component';
import { TodoComponent } from './todo/todo.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { CodesComponent } from './codes/codes.component';
import { Routes,RouterModule } from '@angular/router';
import { UsertodosComponent } from './usertodos/usertodos.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    NavComponent,
    TodosComponent,
    TodoComponent,
    RegistrationComponent,
    LoginComponent,
    CodesComponent,
    UsertodosComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot([//הגדרת הראוטים
      {path:'',component:TodosComponent},
      {path:'register',component:RegistrationComponent},
      {path:'login',component:LoginComponent},
      {path:'codes',component:CodesComponent},
      {path:'usertodos',component:UsertodosComponent},
      {path:'**',component:TodosComponent}


    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
